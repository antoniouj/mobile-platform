package coursework.gcu.antoniouj.org.application.util;

import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import coursework.gcu.antoniouj.org.application.model.CurrentIncidents;
import coursework.gcu.antoniouj.org.application.model.PlannedRoadworks;
import coursework.gcu.antoniouj.org.application.model.TrafficScotlandModel;

/**
 * Created by antoniouj S1429297
 */
public class XMLPullParser {

    public List<TrafficScotlandModel> parseData(String data, String activityType) throws ParseException {

        final String MY_TAG = "XML_PULL_PARSER";

        final String CURRENT_INCIDENTS = "CURRENT_INCIDENTS";
        final String PLANNED_ROADWORKS = "PLANNED_ROADWORKS";

        List<TrafficScotlandModel> trafficScotlandModelList = new ArrayList<>();
        TrafficScotlandModel trafficScotlandModel = null;

        if (activityType.equalsIgnoreCase(PLANNED_ROADWORKS)) {
            trafficScotlandModel = new PlannedRoadworks();
            trafficScotlandModel.setType(PLANNED_ROADWORKS);
        } else if (activityType.equalsIgnoreCase(CURRENT_INCIDENTS)) {
            trafficScotlandModel = new CurrentIncidents();
            trafficScotlandModel.setType(CURRENT_INCIDENTS);
        }

        Log.d(MY_TAG, "Parsing starting");

        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(data));
            int eventType = xpp.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT) {

                if (eventType == XmlPullParser.START_TAG) {

                    if (xpp.getName().equalsIgnoreCase("item")) {
                        if (activityType.equalsIgnoreCase(PLANNED_ROADWORKS)) {
                            trafficScotlandModel = new PlannedRoadworks();
                            trafficScotlandModel.setType(PLANNED_ROADWORKS);
                        } else if (activityType.equalsIgnoreCase(CURRENT_INCIDENTS)) {
                            trafficScotlandModel = new CurrentIncidents();
                            trafficScotlandModel.setType(CURRENT_INCIDENTS);
                        }
                    } else if (xpp.getName().equalsIgnoreCase("title")) {
                        trafficScotlandModel.setTitle(xpp.nextText());
                    } else if (xpp.getName().equalsIgnoreCase("description")) {
                        if (activityType.equalsIgnoreCase(PLANNED_ROADWORKS)) {
                            trafficScotlandModel = extractDescription(xpp.nextText(), trafficScotlandModel);
                        } else if (activityType.equalsIgnoreCase(CURRENT_INCIDENTS)) {
                            trafficScotlandModel.setDescription(xpp.nextText());
                        }
                    } else if (xpp.getName().equalsIgnoreCase("link")) {
                        trafficScotlandModel.setLink(xpp.nextText());
                    } else if (xpp.getName().equalsIgnoreCase("point")) {
                        trafficScotlandModel.setGeo(xpp.nextText());
                    } else if (xpp.getName().equalsIgnoreCase("pubDate")) {
                        trafficScotlandModel.setPublishDate(xpp.nextText());
                    }

                } else if (eventType == XmlPullParser.END_TAG) {
                    if (xpp.getName().equalsIgnoreCase("item")) {
                        trafficScotlandModelList.add(trafficScotlandModel);
                    }
                }
                eventType = xpp.next();
            }

            System.out.println("End document");
            Log.d(MY_TAG, "Parsing finished");

        } catch (XmlPullParserException e) {

            Log.e(MY_TAG, "Parsing error" + e.toString());
        } catch (IOException ae1) {

            Log.e(MY_TAG, "IO error during parsing");
        }

        return trafficScotlandModelList;
    }

    /**
     * Extracts the Work, Traffic Management, Diversion Management, Start Date and End Date from the description.
     * It then calculates the Duration using the Start Date and End Date
     *
     * @param description          the description of the planned roadworks
     * @param trafficScotlandModel the model in which to set the values
     * @return trafficScotlandModel with the values that were extracted and calculated
     * @throws ParseException
     */
    private TrafficScotlandModel extractDescription(String description, TrafficScotlandModel trafficScotlandModel) throws ParseException {

        String formattedDescription = description.replaceAll("\\n", " ");

        Pattern durationPattern = Pattern.compile("Start Date:.*,\\s(.*?)\\s-.*<br />End Date:.*,\\s(.*?)\\s-.*<br />");
        Matcher durationMatcher = durationPattern.matcher(formattedDescription);

        Pattern descriptionPattern = Pattern.compile(".*(Works:.*)\\s(Traffic Management:.*?)\\s?(Diversion Information:.*)?$");
        Matcher descriptionMatcher = descriptionPattern.matcher(formattedDescription);

        PlannedRoadworks plannedRoadworksModel = (PlannedRoadworks) trafficScotlandModel;
        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");

        if (durationMatcher.find()) {

            Date startDate = format.parse(durationMatcher.group(1));
            Date endDate = format.parse(durationMatcher.group(2));

            long diff = endDate.getTime() - startDate.getTime();
            long days = diff / (24 * 60 * 60 * 1000);

            plannedRoadworksModel.setDuration(days);
            plannedRoadworksModel.setStartDate(startDate);
            plannedRoadworksModel.setEndDate(endDate);
        }

        if (descriptionMatcher.find()) {
            plannedRoadworksModel.setWorks(descriptionMatcher.group(1));
            plannedRoadworksModel.setTrafficManagement(descriptionMatcher.group(2));
            plannedRoadworksModel.setDiversion(descriptionMatcher.group(3));
        }

        return plannedRoadworksModel;
    }
}
