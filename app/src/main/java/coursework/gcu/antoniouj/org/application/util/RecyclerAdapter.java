package coursework.gcu.antoniouj.org.application.util;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import coursework.gcu.antoniouj.org.application.CurrentIncidentsViewActivity;
import coursework.gcu.antoniouj.org.application.PlannedRoadworksActivity;
import coursework.gcu.antoniouj.org.application.PlannedRoadworksViewActivity;
import coursework.gcu.antoniouj.org.application.R;
import coursework.gcu.antoniouj.org.application.model.PlannedRoadworks;
import coursework.gcu.antoniouj.org.application.model.TrafficScotlandModel;

/**
 * Created by antoniouj S1429297
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {

    private static final String MY_TAG = "RECYCLER_ADAPTER";

    final String CURRENT_INCIDENTS = "CURRENT_INCIDENTS";
    final String PLANNED_ROADWORKS = "PLANNED_ROADWORKS";

    private List<TrafficScotlandModel> trafficScotlandList;
    private Context context;

    public RecyclerAdapter(List<TrafficScotlandModel> trafficScotlandList, Context context) {

        this.trafficScotlandList = trafficScotlandList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout, parent, false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {

        final TrafficScotlandModel trafficScotlandModel = trafficScotlandList.get(position);

        if (trafficScotlandModel.getType() == PLANNED_ROADWORKS) {
            long duration = ((PlannedRoadworks) trafficScotlandModel).getDuration();

            if (duration <= 1) {
                holder.image.setImageResource(R.mipmap.ic_green_warning);
            } else if (duration > 1 && duration < 7) {
                holder.image.setImageResource(R.mipmap.ic_amber_warning);
            } else if (duration > 7) {
                holder.image.setImageResource(R.mipmap.ic_red_warning);
            }
        } else if (trafficScotlandModel.getType() == CURRENT_INCIDENTS) {
            holder.image.setImageResource(R.mipmap.ic_red_warning);
        }

        holder.title.setText(trafficScotlandModel.getTitle());

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (trafficScotlandModel.getType().equalsIgnoreCase(CURRENT_INCIDENTS)) {

                    Intent currentIncidentsIntent = new Intent(context, CurrentIncidentsViewActivity.class);
                    currentIncidentsIntent.putExtra("title", trafficScotlandModel.getTitle());
                    currentIncidentsIntent.putExtra("description", trafficScotlandModel.getDescription());
                    currentIncidentsIntent.putExtra("geo", trafficScotlandModel.getGeo());

                    context.startActivity(currentIncidentsIntent);
                } else if (trafficScotlandModel.getType().equalsIgnoreCase(PLANNED_ROADWORKS)) {

                    Intent plannedRoadworksIntent = new Intent(context, PlannedRoadworksViewActivity.class);
                    plannedRoadworksIntent.putExtra("title", trafficScotlandModel.getTitle());
                    plannedRoadworksIntent.putExtra("description", trafficScotlandModel.getDescription());
                    plannedRoadworksIntent.putExtra("geo", trafficScotlandModel.getGeo());
                    plannedRoadworksIntent.putExtra("work", ((PlannedRoadworks) trafficScotlandModel).getWorks());
                    plannedRoadworksIntent.putExtra("trafficManagement", ((PlannedRoadworks) trafficScotlandModel).getTrafficManagement());
                    plannedRoadworksIntent.putExtra("diversionInformation", ((PlannedRoadworks) trafficScotlandModel).getDiversion());

                    context.startActivity(plannedRoadworksIntent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return trafficScotlandList.size();
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView title;

        RelativeLayout parentLayout;

        public RecyclerViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            title = view.findViewById(R.id.title);
            parentLayout = view.findViewById(R.id.parent_layout);
        }

    }

    public void setFilter(List<TrafficScotlandModel> filteredList) {

        trafficScotlandList = new ArrayList<>();
        trafficScotlandList.addAll(filteredList);
        notifyDataSetChanged();
    }
}
