package coursework.gcu.antoniouj.org.application;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

/**
 * Created by antoniouj S1429297
 */
public class MainActivity extends AppCompatActivity {

    private static final String MY_TAG = "MAIN_ACTIVITY";

    private final String CURRENT_INCIDENTS = "CURRENT_INCIDENTS";
    private final String PLANNED_ROADWORKS = "PLANNED_ROADWORKS";
    private final String ACTIVITY = "ACTIVITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(MY_TAG, "Being Created");
    }

    /**
     * Called when the Current Incidents button is clicked
     *
     * @param view
     */
    public void currentIncidents(View view) {
        Intent currentIncidentsIntent = new Intent(MainActivity.this, CurrentIncidentsActivity.class);
        currentIncidentsIntent.putExtra(ACTIVITY, CURRENT_INCIDENTS);
        startActivity(currentIncidentsIntent);
    }

    /**
     * Called when the Planned Roadworks button is clicked
     *
     * @param view
     */
    public void plannedRoadworks(View view) {
        Intent plannedRoadworksIntent = new Intent(MainActivity.this, PlannedRoadworksActivity.class);
        plannedRoadworksIntent.putExtra(ACTIVITY, PLANNED_ROADWORKS);
        startActivity(plannedRoadworksIntent);
    }
}
