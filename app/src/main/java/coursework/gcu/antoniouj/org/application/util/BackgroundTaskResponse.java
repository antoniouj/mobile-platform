package coursework.gcu.antoniouj.org.application.util;

import java.util.List;

import coursework.gcu.antoniouj.org.application.model.TrafficScotlandModel;

/**
 * Created by antoniouj S1429297
 */

/**
 *  Interface to capture the response from the AsyncTask {@link BackgroundTask}
 */
public interface BackgroundTaskResponse {
    void processResponse(List<TrafficScotlandModel> response);
}
