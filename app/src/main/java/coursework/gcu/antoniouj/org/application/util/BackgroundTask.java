package coursework.gcu.antoniouj.org.application.util;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.util.List;

import coursework.gcu.antoniouj.org.application.model.TrafficScotlandModel;

/**
 * Created by antoniouj S1429297
 */
public class BackgroundTask extends AsyncTask<Void, List<TrafficScotlandModel>, List<TrafficScotlandModel>> {

    private static final String MY_TAG = "BACKGROUND_TASK";

    private String url;
    private String activityType;

    public BackgroundTaskResponse delegate;

    private XMLPullParser xmlPullParser = new XMLPullParser();

    public BackgroundTask(String url, String activityType) {
        this.url = url;
        this.activityType = activityType;
    }

    @Override
    protected List<TrafficScotlandModel> doInBackground(Void... voids) {

        List<TrafficScotlandModel> parsedData = null;

        try {
            URL convertedUrl = new URL(url);
            URLConnection urlConnection = convertedUrl.openConnection();
            InputStream inputStream = urlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }

            String data = stringBuilder.toString();

            try {
                parsedData = xmlPullParser.parseData(data, activityType);
            } catch (ParseException e) {
                Log.e(MY_TAG, e.toString());
            }

        } catch (IOException e) {
            Log.e(MY_TAG, e.toString());
        }
        return parsedData;
    }

    @Override
    protected void onPostExecute(List<TrafficScotlandModel> trafficScotlandModelList) {
        delegate.processResponse(trafficScotlandModelList);
    }
}
