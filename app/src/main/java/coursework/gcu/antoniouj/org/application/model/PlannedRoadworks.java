package coursework.gcu.antoniouj.org.application.model;

/**
 * Created by antoniouj S1429297
 */

import java.util.Date;

/**
 * Planned roadworks models which extends abstract {@link TrafficScotlandModel}
 */
public class PlannedRoadworks extends TrafficScotlandModel {

    private long duration;
    private String works;
    private String trafficManagement;
    private String diversion;
    private Date startDate;
    private Date endDate;

    public long getDuration() {
        return duration;
    }


    public void setDuration(long duration) {
        this.duration = duration;
    }


    public String getWorks() {
        return works;
    }

    public void setWorks(String works) {
        this.works = works;
    }

    public String getTrafficManagement() {
        return trafficManagement;
    }

    public void setTrafficManagement(String trafficManagement) {
        this.trafficManagement = trafficManagement;
    }

    public String getDiversion() {
        return diversion;
    }

    public void setDiversion(String diversion) {
        this.diversion = diversion;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
