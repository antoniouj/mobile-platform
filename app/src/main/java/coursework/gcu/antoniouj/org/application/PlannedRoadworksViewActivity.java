package coursework.gcu.antoniouj.org.application;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.SimpleDateFormat;

/**
 * Created by antoniouj S1429297
 */
public class PlannedRoadworksViewActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String MY_TAG = "PLANNED_ROADWORKS_VIEW";

    private String title;
    private double lat;
    private double lon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planned_roadworks_view);

        Log.d(MY_TAG, "Being Created");

        getIncomingIntent();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        GoogleMap map = googleMap;

        LatLng location = new LatLng(lat, lon);

        map.addMarker(new MarkerOptions()
                .title(title)
                .position(location));

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 13));

    }

    /**
     * Gets incoming intents and validate the values
     */
    private void getIncomingIntent() {

        Log.d(MY_TAG, "Incoming Intents");

        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");

        if (getIntent().hasExtra("title")
                && getIntent().hasExtra("work")
                && getIntent().hasExtra("trafficManagement")
                && getIntent().hasExtra("diversionInformation")
                && getIntent().hasExtra("geo")) {

            title = getIntent().getStringExtra("title");
            String work = getIntent().getStringExtra("work");
            String trafficManagement = getIntent().getStringExtra("trafficManagement");
            String diversionInformation = getIntent().getStringExtra("diversionInformation");
            extractGeoPoints(getIntent().getStringExtra("geo"));

            setView(title, work, trafficManagement, diversionInformation);
        }
    }

    /**
     * Sets the values recieved from the intent on the activity
     *
     * @param title                the title of the roadwork
     * @param work                 the work being done
     * @param trafficManagement    the management for the work
     * @param diversionInformation diversion information for the roadwork
     */
    private void setView(String title, String work, String trafficManagement, String diversionInformation) {

        TextView titleView = findViewById(R.id.title);
        titleView.setText(title);

        TextView workView = findViewById(R.id.work);
        workView.setText(work);

        TextView trafficManagementView = findViewById(R.id.traffic_management);
        trafficManagementView.setText(trafficManagement);

        TextView diversionInformationView = findViewById(R.id.diversion_information);
        diversionInformationView.setText(diversionInformation);
    }

    /**
     * Extracts the longitude and latitude
     *
     * @param geo the string to extract the points from
     */
    private void extractGeoPoints(String geo) {

        String[] geoPoints = geo.split("\\s+");

        lat = Double.parseDouble(geoPoints[0]);
        lon = Double.parseDouble(geoPoints[1]);
    }
}
