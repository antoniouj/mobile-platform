package coursework.gcu.antoniouj.org.application;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by antoniouj S1429297
 */
public class CurrentIncidentsViewActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String MY_TAG = "CURRENT_INCIDENTS_VIEW";

    private String title;
    private String description;
    private double lat;
    private double lon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_incidents_view);

        Log.d(MY_TAG, "Being Created");

        getIncomingIntent();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        GoogleMap map = googleMap;

        LatLng location = new LatLng(lat, lon);

        map.addMarker(new MarkerOptions()
                .title(title)
                .snippet(description)
                .position(location));

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 13));

    }

    /**
     * Gets incoming intents and validate the values
     */
    private void getIncomingIntent() {

        Log.d(MY_TAG, "Incoming Intents");

        if (getIntent().hasExtra("title") && getIntent().hasExtra("description") && getIntent().hasExtra("geo")) {
            title = getIntent().getStringExtra("title");
            description = getIntent().getStringExtra("description");
            extractGeoPoints(getIntent().getStringExtra("geo"));

            setView(title, description);
        }
    }

    /**
     * Sets the values recieved from the intent on the activity
     *
     * @param title       the title of the roadwork
     * @param description description of the current incident
     */
    private void setView(String title, String description) {

        TextView titleView = findViewById(R.id.title);
        titleView.setText(title);

        TextView descriptionView = findViewById(R.id.description);
        descriptionView.setText(description);
    }

    /**
     * Extracts the longitude and latitude
     *
     * @param geo the string to extract the points from
     */
    private void extractGeoPoints(String geo) {

        String[] geoPoints = geo.split("\\s+");

        lat = Double.parseDouble(geoPoints[0]);
        lon = Double.parseDouble(geoPoints[1]);
    }
}
