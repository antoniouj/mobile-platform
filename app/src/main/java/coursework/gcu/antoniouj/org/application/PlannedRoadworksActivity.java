package coursework.gcu.antoniouj.org.application;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.DatePicker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import coursework.gcu.antoniouj.org.application.model.PlannedRoadworks;
import coursework.gcu.antoniouj.org.application.model.TrafficScotlandModel;
import coursework.gcu.antoniouj.org.application.util.BackgroundTask;
import coursework.gcu.antoniouj.org.application.util.BackgroundTaskResponse;
import coursework.gcu.antoniouj.org.application.util.DatePickerFragment;
import coursework.gcu.antoniouj.org.application.util.RecyclerAdapter;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;

/**
 * Created by antoniouj S1429297
 */
public class PlannedRoadworksActivity extends AppCompatActivity implements BackgroundTaskResponse, SearchView.OnQueryTextListener, DatePickerDialog.OnDateSetListener {

    private static final String MY_TAG = "PLANNED_ROADWORKS";

    private final String plannedRoadworksUrl = "http://trafficscotland.org/rss/feeds/plannedroadworks.aspx";
    private final String ACTIVITY = "ACTIVITY";

    private List<TrafficScotlandModel> trafficScotlandList = new ArrayList<>();
    private static String activityType = null;

    RecyclerView recyclerView;
    RecyclerAdapter adapter;
    RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planned_roadworks);

        Log.d(MY_TAG, "Being Created");

        initRecyclerView();

        getIncomingIntent();

        BackgroundTask backgroundTask = new BackgroundTask(plannedRoadworksUrl, activityType);
        backgroundTask.delegate = this;
        backgroundTask.execute();
    }

    @Override
    public void processResponse(List<TrafficScotlandModel> response) {
        trafficScotlandList.addAll(response);
        adapter.notifyDataSetChanged();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        initSearch(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.datepicker:
                openDatePicker();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        titleFilter(newText);
        return true;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);

        Date date = calendar.getTime();

        dateFilter(date);
    }

    private void openDatePicker() {
        DialogFragment datePicker = new DatePickerFragment();
        datePicker.show(getSupportFragmentManager(), "date picker");
    }

    /**
     * Initialises the Recycler View Adapter
     */
    private void initRecyclerView() {

        Log.d(MY_TAG, "Recycler View Initialising");

        recyclerView = findViewById(R.id.recyclerView);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, VERTICAL);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter(trafficScotlandList, this);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(dividerItemDecoration);
    }

    /**
     * Initialises the SearchView within the menu
     *
     * @param menu the menu in which the search view is used
     */
    private void initSearch(Menu menu) {

        Log.d(MY_TAG, "Search Initialising");

        MenuItem menuItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) menuItem.getActionView();

        int options = searchView.getImeOptions();
        searchView.setImeOptions(options | EditorInfo.IME_FLAG_NO_EXTRACT_UI);

        searchView.setOnQueryTextListener(this);
    }

    /**
     * Gets incoming intents and validate the values
     */
    private void getIncomingIntent() {

        Log.d(MY_TAG, "Incoming Intents");

        if (getIntent().hasExtra(ACTIVITY)) {
            activityType = getIntent().getStringExtra(ACTIVITY);
        }
    }

    /**
     * Filters the trafficScotlandList by Title
     *
     * @param newText the text entered in the SearchView
     * @return true when success
     */
    private boolean titleFilter(String newText) {

        String lowerCaseNewText = newText.toLowerCase();

        List<TrafficScotlandModel> filteredTrafficScotlandList = new ArrayList<>();

        for (TrafficScotlandModel trafficScotlandModel : trafficScotlandList) {

            String title = trafficScotlandModel.getTitle().toLowerCase();

            if (title.contains(lowerCaseNewText)) {

                filteredTrafficScotlandList.add(trafficScotlandModel);
            }
        }
        adapter.setFilter(filteredTrafficScotlandList);
        return true;
    }

    /**
     * Filters the trafficScotlandList by Date
     *
     * @param date
     * @return true when success
     */
    private boolean dateFilter(Date date) {

        List<TrafficScotlandModel> filteredTrafficScotlandList = new ArrayList<>();

        for (TrafficScotlandModel trafficScotlandModel : trafficScotlandList) {

            Date startDate = ((PlannedRoadworks) trafficScotlandModel).getStartDate();
            Date endDate = ((PlannedRoadworks) trafficScotlandModel).getEndDate();

            if (date.after(startDate) && date.before(endDate)) {

                filteredTrafficScotlandList.add(trafficScotlandModel);
            }
        }
        adapter.setFilter(filteredTrafficScotlandList);
        return true;
    }
}
