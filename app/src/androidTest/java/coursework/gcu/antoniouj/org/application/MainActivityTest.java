package coursework.gcu.antoniouj.org.application;

import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by antoniouj
 */

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setUp() {
        Intents.init();
    }

    @After
    public void tearDown() {
        Intents.release();
    }

    @Test
    public void clickCurrentIncidentsButton_OpensCurrentIncidentsActivity() {
        onView(withId(R.id.current_incidents))
                .perform(click());

        intended(hasComponent(CurrentIncidentsActivity.class.getName()));
    }

    @Test
    public void clickPlannedRoadworksButton_OpensPlannedRoadworksActivity() {
        onView(withId(R.id.planned_roadworks))
                .perform(click());

        intended(hasComponent(PlannedRoadworksActivity.class.getName()));
    }
}
